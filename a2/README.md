# LIS3781 - Advanced Database Management

## Benjamin Landerman

### Assignment 2 Requirements:

#### README.md file should include the following items:

* Screenshot of SQL Code.
* Screenshot of populated tables.

#### Assignment Screenshots:

| *Screenshot of SQL code*                      | *Screenshot of Populated Tables*              |
|:---------------------------------------------:|:---------------------------------------------:|
|![SQL](img/a2_sql.png)                         |![Tables](img/a2_tables.png)                   |

#### SQL Links:

[A2 SQL](docs/a2_sql.sql "Link to A2 SQL")

[A2 SQL Statements](docs/a2_sql_solutions.sql "Link to A2 SQL Statements")
-- Granting Privileges
/* Note: as of MySQL 5.7.6, using GRANT for creating new user is deprecated and will be
removed in future release.
Instead, create new user with CREATE USER statement, then use GRANT statement:
a) CREATE USER 'myuser'@'localhost' IDENTIFIED BY 'password';
b) GRANT select, update, delete on mydb.company to 'myuser'@'localhost'; */

-- The following still work, but are deprecated!
/* 1) Grant select, update, delete
 on bdl16.company
 to user3@"localhost"
 identified by'test3';
  show warnings;

GRANT select, update, delete
 on bdl16.customer
 to user3@"localhost"
 identified by 'test3';
  show warnings;

Grant select, update, delete
 on bdl16.*
 to user3@"localhost"
 identified by 'test3';
  show warnings; */

-- 1) create user
CREATE USER 'user3'@'localhost' IDENTIFIED BY 'test3';
-- 2) create permissions
GRANT select, update, delete on bdl16.* to 'user3'@'localhost';

-- 2)
Grant select, insert
 on bdl16.customer
 to user4@"localhost"
 identified by 'test4';
  show warnings;

FLUSH PRIVILEGES;

-- 3)
show grants; -- displays grants for current user
show grants for 'user3'@'localhost'; -- displays grants for selected user

-- 4)
select user(), version();

-- 5)
use bdl16;
show tables;

-- 6) 
describe company;
describe customer;

-- 7) 
select * from company;
select * from customer;

-- 8)
update company set cmp_id = 6 where cmp_id = 1;
select * from company;
select * from customer;

-- 9)
delete from company where cmp_id = 6;

-- 10a)
INSERT INTO company VALUES 

(null,'C-Corp','507 - 20th Ave. E. Apt. 2A','Seattle','WA','981226749','2065559857','12345678.00','http://www.http://technologies.ci.fsu.edu/node/72');

-- error: ERROR 1142 (42000): INSERT command denied to user 'user3'@'localhost' for table 'company'

-- 10b)
INSERT INTO customer VALUES 

(null,2,'Discount','Wilbur','Denaway','23 Billings Gate','El Paso','TX','885703412','2145559857','test1@mymail.com','8391.87','37642.00');
-- error: ERROR 1142 (42000): INSERT command denied to user 'user3'@'localhost' for table 'customer'

-- 11a)
show grants;
show tables;
select * from company;

-- 11b)
delete from customer;

-- 12)
drop table customer;
drop table company;
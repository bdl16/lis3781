# LIS3781 - Advanced Database Management

## Benjamin Landerman

### Assignment 4 Requirements:

*Assignment Requirements:*

1. Create tables with data in MS SQL Server
2. 10+ inserts for person table, 5 inserts for remaining tables
3. SQL solutions for required reports

#### README.md file should include the following items:

* Screenshots of ERD
* Links to SQL files

#### Assignment Screenshots:

| *Screenshot of A4 ERD*                        |
|:---------------------------------------------:|
|![A4 ERD](img/a4_erd.png)                      |

#### Assignment Links:

[A4 SQL](docs/a4_database.sql "Link to A4 SQL")

[A4 SQL Statements](docs/a4_sql_solutions.sql "Link to A4 SQL Statements")
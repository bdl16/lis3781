# LIS3781 - Advanced Database Management

## Benjamin Landerman

### Assignment 3 Requirements:

*Assignment Requirements:*

1. Create tables with data in Oracle environment
2. SQL solutions for the 25 required reports

#### README.md file should include the following items:

* Screenshots of SQL code
* Screenshot of populated tables in Oracle
* SQL code for required reports

#### Assignment Screenshots:

| *Screenshot of SQL code*                      | *Screenshot of populated tables*              |
|:---------------------------------------------:|:---------------------------------------------:|
|![SQL](img/sqlcode.png)                        |![Tables](img/tables.png)                      |

#### Assignment Links:

[A3 SQL File](docs/oracle_estore.sql "Link to A3 SQL")

[A3 SQL Solutions](docs/a3_sql_solutions.sql "Link to A3 SQL Solutions")
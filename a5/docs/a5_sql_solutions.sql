-- a5 sql solutions
-- 1) Create a stored procedure (product_days_of_week) listingthe product names, descriptions,and the day of the week in which they were sold, in ascending order of the day of week.


-- 2) Create a stored procedure (product_drill_down) listingthe product name, quantity on hand, store name, city name, state name, and region name where each product was purchased, in descending order of quantity on hand.


-- 3) Create a stored procedure(add_payment)that adds a payment record. Use variables and pass suitable arguments.


-- 4) Create a stored procedure (customer_balance) listing the customer’s id, name, invoice id, total paid on invoice, balance (derived attribute from the difference of a customer’s invoice total and theirrespective payments), pass customer’s last name as argument—which may return more than one value.


-- 5) Create and display the results of a stored procedure (store_sales_between_dates) that lists each store's id, sum of total sales (formatted), and years for a given time period, by passing the start/end dates, group by years, and sort by totalsales then years, both in descending order.


-- 6) Create a trigger (trg_check_inv_paid) that updates an invoice record, after a payment has been made, indicating whether or not the invoice has been paid.


-- Extra Credit: Create anddisplay the results of a stored procedure (order_line_total) that calculates the total price for each order line, based upon the product price times quantity, which yields a subtotal (oln_price), totalcolumnincludes6% sales tax. Query result set shoulddisplay order line id, product id, name, description, price, order line quantity,subtotal(oln_price), and totalwith 6% sales tax.Sort by product ID.
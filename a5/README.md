# LIS3781 - Advanced Database Management

## Benjamin Landerman

### Assignment 5 Requirements:

*Assignment Requirements:*

1. Create tables with data in MS SQL Server
2. 25 unique records in sale table and 5 in all other tables
3. SQL solutions for required reports

#### README.md file should include the following items:

* Screenshots of ERD
* Links to SQL files

#### Assignment Screenshots:

| *Screenshot of A5 ERD*                        |
|:---------------------------------------------:|
|![A5 ERD](img/a5_erd.png)                      |

#### Assignment Links:

[A5 SQL](docs/a5_database.sql "Link to A5 SQL")

[A5 SQL Statements](docs/a5_sql_solutions.sql "Link to A5 SQL Statements")
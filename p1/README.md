# LIS3781 - Advanced Database Management

## Benjamin Landerman

### Project 1 Requirements:

*Assignment Requirements:*

1. Create database to track and document city court data
2. SSNs must be hashed and salted
3. P1 SQL statements

#### README.md file should include the following items:

* Screenshot of ERD
* Links to SQL files

#### Assignment Screenshots:

| *Screenshot of P1 ERD*                        |
|:---------------------------------------------:|
|![P1 ERD](img/p1_erd.png)                      |

#### SQL Links:

[P1 MWB](docs/p1_erd.mwb "Link to P1 MWB")

[P1 SQL](docs/p1_database.sql "Link to P1 SQL")

[P1 SQL Statements](docs/p1_sql.sql "Link to P1 SQL Statements")
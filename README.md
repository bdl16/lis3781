# LIS3781 - Advanced Database Management

## Benjamin Landerman

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create tables with data in MySQL
    - Use hashing and salt for SSNs

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create tables with data in Oracle environment
    - SQL solutions for the 25 required reports
    
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create tables with data in MS SQL Server
    - 10+ inserts for person table, 5 inserts for remaining tables
    - SQL solutions for required reports
    
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create tables with data in MS SQL Server
    - 25 unique records in sale table and 5 in all other tables
    - SQL solutions for required reports

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create database to track and document city court data
    - SSNs must be hashed and salted
    - P1 SQL statements

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Download MongoDB
    - Create and connect to cluster
    - Import restaurant collection
    - MongoDB shell commands
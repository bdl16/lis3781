# LIS3781 - Advanced Database Management

## Benjamin Landerman

### Project 2 Requirements:

*Assignment Requirements:*

1. Download MongoDB
2. Create and connect to cluster
3. Import restaurant collection
4. MongoDB shell commands

#### README.md file should include the following items:

* Screenshot of at least one MongoDB shell command

#### Assignment Screenshots:

| *Screenshot of shell command*                 |
|:---------------------------------------------:|
|![shell command](img/cmd.png)                  |
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema bdl16
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `bdl16` ;

-- -----------------------------------------------------
-- Schema bdl16
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bdl16` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `bdl16` ;

-- -----------------------------------------------------
-- Table `bdl16`.`job`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bdl16`.`job` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `bdl16`.`job` (
  `job_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `job_title` VARCHAR(45) NOT NULL,
  `job_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`job_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `bdl16`.`employee`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bdl16`.`employee` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `bdl16`.`employee` (
  `emp_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `job_id` TINYINT UNSIGNED NOT NULL,
  `emp_ssn` INT(9) UNSIGNED NOT NULL,
  `emp_fname` VARCHAR(15) NOT NULL,
  `emp_lname` VARCHAR(30) NOT NULL,
  `emp_dob` DATE NOT NULL,
  `emp_start_date` DATE NOT NULL,
  `emp_end_date` DATE NULL,
  `emp_salary` DECIMAL(8,2) NOT NULL,
  `emp_street` VARCHAR(30) NOT NULL,
  `emp_city` VARCHAR(20) NOT NULL,
  `emp_state` CHAR(2) NOT NULL,
  `emp_zip` INT(9) UNSIGNED NOT NULL,
  `emp_phone` BIGINT UNSIGNED NOT NULL,
  `emp_email` VARCHAR(100) NOT NULL,
  `emp_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`emp_id`),
  INDEX `fk_employee_job1_idx` (`job_id` ASC),
  CONSTRAINT `fk_employee_job1`
    FOREIGN KEY (`job_id`)
    REFERENCES `bdl16`.`job` (`job_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `bdl16`.`dependent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bdl16`.`dependent` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `bdl16`.`dependent` (
  `dep_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `emp_id` SMALLINT UNSIGNED NOT NULL,
  `dep_added` DATE NOT NULL,
  `dep_ssn` INT(9) UNSIGNED NOT NULL,
  `dep_fname` VARCHAR(15) NOT NULL,
  `dep_lname` VARCHAR(30) NOT NULL,
  `dep_dob` DATE NOT NULL,
  `dep_relation` VARCHAR(20) NOT NULL,
  `dep_street` VARCHAR(30) NOT NULL,
  `dep_city` VARCHAR(20) NOT NULL,
  `dep_state` CHAR(2) NOT NULL,
  `dep_zip` INT(9) UNSIGNED NOT NULL,
  `dep_phone` BIGINT UNSIGNED NOT NULL,
  `dep_email` VARCHAR(100) NULL,
  `dep_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`dep_id`),
  INDEX `fk_dependent_employee_idx` (`emp_id` ASC),
  CONSTRAINT `fk_dependent_employee`
    FOREIGN KEY (`emp_id`)
    REFERENCES `bdl16`.`employee` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `bdl16`.`benefit`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bdl16`.`benefit` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `bdl16`.`benefit` (
  `ben_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `ben_name` VARCHAR(45) NOT NULL,
  `ben_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`ben_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `bdl16`.`plan`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bdl16`.`plan` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `bdl16`.`plan` (
  `pln_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `emp_id` SMALLINT UNSIGNED NOT NULL,
  `ben_id` TINYINT UNSIGNED NOT NULL,
  `pln_type` ENUM('single', 'spouse', 'family') NOT NULL,
  `pln_cost` DECIMAL(6,2) NOT NULL,
  `pln_election_date` DATE NOT NULL,
  `pln_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pln_id`),
  INDEX `fk_plan_employee1_idx` (`emp_id` ASC),
  INDEX `fk_plan_benefit1_idx` (`ben_id` ASC),
  CONSTRAINT `fk_plan_employee1`
    FOREIGN KEY (`emp_id`)
    REFERENCES `bdl16`.`employee` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_plan_benefit1`
    FOREIGN KEY (`ben_id`)
    REFERENCES `bdl16`.`benefit` (`ben_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `bdl16`.`emp_hist`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bdl16`.`emp_hist` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `bdl16`.`emp_hist` (
  `eht_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `emp_id` SMALLINT UNSIGNED NULL,
  `eht_date` DATETIME NOT NULL,
  `eht_type` ENUM('i', 'u', 'd') NOT NULL,
  `eht_job_id` TINYINT UNSIGNED NOT NULL,
  `eht_emp_salary` DECIMAL(8,2) NOT NULL,
  `eht_user_changed` VARCHAR(30) NOT NULL,
  `eht_reason` VARCHAR(45) NOT NULL,
  `eht_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`eht_id`),
  INDEX `fk_emp_history_employee1_idx` (`emp_id` ASC),
  CONSTRAINT `fk_emp_history_employee1`
    FOREIGN KEY (`emp_id`)
    REFERENCES `bdl16`.`employee` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `bdl16`.`job`
-- -----------------------------------------------------
START TRANSACTION;
USE `bdl16`;
INSERT INTO `bdl16`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'Director', 'notes');
INSERT INTO `bdl16`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'Manager', NULL);
INSERT INTO `bdl16`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'Associate', NULL);
INSERT INTO `bdl16`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'Manager', '12513');
INSERT INTO `bdl16`.`job` (`job_id`, `job_title`, `job_notes`) VALUES (DEFAULT, 'President', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `bdl16`.`employee`
-- -----------------------------------------------------
START TRANSACTION;
USE `bdl16`;
INSERT INTO `bdl16`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (DEFAULT, 5, 547855369, 'Joe', 'Wallace', '1975-05-12', '1995-12-01', NULL, 123546.52, '145 North Bend', 'Tallahassee', 'FL', 323044566, 8506558946, 'jwallace@aol.com', '1234');
INSERT INTO `bdl16`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (DEFAULT, 4, 521489625, 'Terrence', 'Smith', '1980-10-24', '2001-06-12', '2014-08-25', 85252.12, '826 Green Avenue', 'Tallahassee', 'FL', 323031256, 8507651865, 'terrences@gmail.com', NULL);
INSERT INTO `bdl16`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (DEFAULT, 1, 245987456, 'Kylie', 'Howell', '1978-02-20', '1999-05-16', NULL, 124785.00, '142 Oranole Road', 'Orlando', 'FL', 327148456, 4078542365, 'khowell@aol.com', 'notes');
INSERT INTO `bdl16`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (DEFAULT, 2, 885475696, 'Tim', 'Johnson', '1984-01-24', '2005-11-22', '2018-03-14', 132185.00, '914 Blue Drive', 'Orlando', 'FL', 327891856, 4075132569, 'timjohnson@gmail.com', NULL);
INSERT INTO `bdl16`.`employee` (`emp_id`, `job_id`, `emp_ssn`, `emp_fname`, `emp_lname`, `emp_dob`, `emp_start_date`, `emp_end_date`, `emp_salary`, `emp_street`, `emp_city`, `emp_state`, `emp_zip`, `emp_phone`, `emp_email`, `emp_notes`) VALUES (DEFAULT, 3, 521456985, 'Morgan', 'Walker', '1978-06-18', '1999-12-03', NULL, 142356.50, '145 Alto Road', 'Tallahassee', 'FL', 323041485, 8504669856, 'morganw@aol.com', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `bdl16`.`dependent`
-- -----------------------------------------------------
START TRANSACTION;
USE `bdl16`;
INSERT INTO `bdl16`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (DEFAULT, 2, '2008-10-15', 123456789, 'Josh', 'Smith', '1997-05-21', 'Son', '148 South Road', 'Tallahassee', 'FL', 323037452, 8505963128, 'joshs@gmail.com', 'abcabc');
INSERT INTO `bdl16`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (DEFAULT, 3, '2015-05-22', 123485695, 'Ryan', 'Howell', '1999-12-05', 'Son', '255 West Street', 'Tallahassee', 'FL', 323044853, 8501579866, 'ryan@aol.com', NULL);
INSERT INTO `bdl16`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (DEFAULT, 1, '2017-10-05', 123456756, 'Jessica', 'Wallace', '2001-02-14', 'Daughter', '985 Orange Avenue', 'Orlando', 'FL', 327142564, 4076851358, NULL, NULL);
INSERT INTO `bdl16`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (DEFAULT, 4, '2012-04-19', 325697452, 'Rachael', 'Johnson', '1998-12-14', 'Daughter', '124 Ward Road', 'Altamonte Springs', 'FL', 327143659, 4078542635, 'rachaelw@gmail.com', 'notes123');
INSERT INTO `bdl16`.`dependent` (`dep_id`, `emp_id`, `dep_added`, `dep_ssn`, `dep_fname`, `dep_lname`, `dep_dob`, `dep_relation`, `dep_street`, `dep_city`, `dep_state`, `dep_zip`, `dep_phone`, `dep_email`, `dep_notes`) VALUES (DEFAULT, 5, '2009-06-08', 532478569, 'John', 'Walker', '1996-12-02', 'Son', '547 Old Drive', 'Tallahassee', 'FL', 323041256, 8504658796, NULL, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `bdl16`.`benefit`
-- -----------------------------------------------------
START TRANSACTION;
USE `bdl16`;
INSERT INTO `bdl16`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (DEFAULT, 'Vision', NULL);
INSERT INTO `bdl16`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (DEFAULT, 'Dental', NULL);
INSERT INTO `bdl16`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (DEFAULT, 'Health', 'All');
INSERT INTO `bdl16`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (DEFAULT, 'Retirement', NULL);
INSERT INTO `bdl16`.`benefit` (`ben_id`, `ben_name`, `ben_notes`) VALUES (DEFAULT, 'Life', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `bdl16`.`plan`
-- -----------------------------------------------------
START TRANSACTION;
USE `bdl16`;
INSERT INTO `bdl16`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (DEFAULT, 3, 1, 'single', 4563.50, '2010-12-05', 'notes!');
INSERT INTO `bdl16`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (DEFAULT, 2, 4, 'spouse', 8502.00, '2012-05-10', NULL);
INSERT INTO `bdl16`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (DEFAULT, 1, 5, 'family', 1085.50, '2009-06-12', NULL);
INSERT INTO `bdl16`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (DEFAULT, 4, 3, 'family', 4305.00, '2011-09-10', NULL);
INSERT INTO `bdl16`.`plan` (`pln_id`, `emp_id`, `ben_id`, `pln_type`, `pln_cost`, `pln_election_date`, `pln_notes`) VALUES (DEFAULT, 5, 2, 'single', 7506.50, '2010-12-18', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `bdl16`.`emp_hist`
-- -----------------------------------------------------
START TRANSACTION;
USE `bdl16`;
INSERT INTO `bdl16`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_user_changed`, `eht_reason`, `eht_notes`) VALUES (DEFAULT, 2, '2008-12-01 05:55:22', 'i', 10, 50250.80, 'Chris', 'Promotion', NULL);
INSERT INTO `bdl16`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_user_changed`, `eht_reason`, `eht_notes`) VALUES (DEFAULT, 3, '2010-05-15 01:25:52', 'u', 200, 112345.00, 'Ian', 'New Positon', NULL);
INSERT INTO `bdl16`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_user_changed`, `eht_reason`, `eht_notes`) VALUES (DEFAULT, 5, '2009-08-12 03:12:33', 'd', 152, 100235.50, 'Jimmy', 'New Benefits', NULL);
INSERT INTO `bdl16`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_user_changed`, `eht_reason`, `eht_notes`) VALUES (DEFAULT, 1, '2007-10-15 12:22:45', 'd', 189, 89652.00, 'Sam', 'Promotion', NULL);
INSERT INTO `bdl16`.`emp_hist` (`eht_id`, `emp_id`, `eht_date`, `eht_type`, `eht_job_id`, `eht_emp_salary`, `eht_user_changed`, `eht_reason`, `eht_notes`) VALUES (DEFAULT, 4, '2011-02-18 12:35:22', 'u', 75, 148228.50, 'Jared', 'Promotion', 'notes@');

COMMIT;


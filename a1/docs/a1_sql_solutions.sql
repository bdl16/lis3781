-- A1 SQL Solutions

-- 1) a.list (current) jobtitleeach employee has, b.includename, c.address, d.phone,e.SSN, f.order by last name in descending order, g.use old-style join. 
select emp_id, emp_fname, emp_lname,
 CONCAT(emp_street, ", ", emp_city, ", ", emp_state, " ", substring(emp_zip,1,5), '-', substring(emp_zip,6,4)) as address,
 CONCAT('(', substring(emp_phone,1,3), ')', substring(emp_phone,4,3), '-', substring(emp_phone,7,4)) as phone_num,
 CONCAT(substring(emp_ssn,1,3), '-', substring(emp_ssn,4,2), '-', substring(emp_ssn,6,4)) as emp_ssn, job_title
from job as j, employee as e
where j.job_id = e.job_id
order by emp_lname desc;

-- 2) List all job titles and salaries each employee HAS and HAD, include employee ID, full name, job ID, job title, salaries, and respective dates, sort by employee id and date, use old-style join
SELECT e.emp_id, emp_fname, emp_lname, eht_date, eht_job_id, job_title, eht_emp_salary, eht_notes
from employee e, emp_hist h, job j
where e.emp_id = h.emp_id
      and eht_job_id = j.job_id
order by emp_id, eht_date;

-- 3) List employee and dependent full names, DOBs, relationships, and ages of both employee and respective dependent(s), sort by employee last name in ascending order, use naturaljoin
SELECT emp_fname, emp_lname, emp_dob,
TIMESTAMPDIFF(year, emp_dob, curdate()) as emp_age,
dep_fname, dep_lname, dep_relation, dep_dob,
TIMESTAMPDIFF(year, dep_dob, curdate()) AS dep_age
from employee
    NATURAL JOIN dependent
order by emp_lname;

-- 4) Create a transaction that updates job ID 1 to the following title“owner,” w/o the quotation marks, display the job records before and after the change, inside the transaction
START TRANSACTION;
select * from job;

UPDATE job SET job_title='owner' WHERE job_id=1;

select * from job;
COMMIT;

-- 5) Create a stored procedure that adds one record to the benefit table with the following values: benefitname“new benefit,” benefitnotes “testing,” both attribute values w/o the quotation marks, display the benefitrecords before and after the change, inside the stored procedure:
DROP PROCEDURE IF EXISTS insert_benefit;
DELIMITER //
CREATE PROCEDURE insert_benefit()

BEGIN
  SELECT * FROM benefit;
  INSERT INTO benefit 
  (ben_name, ben_notes)
  VALUES
  ('new benefit','testing');
  SELECT * FROM benefit;
END //
DELIMITER ;
CALL insert_benefit();
DROP PROCEDURE IF EXISTS insert_benefit;

-- 6) List employees’and dependents’ names and social security numbers, also include employees’ e-mail addresses, dependents’ mailing addresses, and dependents’ phone numbers. *MUST* display *ALL* employee data, even where there are no associated dependent values. (Major table: all rows displayed, minor table: display null values.)
SELECT 
  concat(emp_lname, ", ", emp_fname) as employee, 
  concat(substring(emp_ssn,1,3), '-', substring(emp_ssn,4,2), '-', substring(emp_ssn,6,4)) as emp_ssn,
  emp_email as email, 
  concat(dep_lname, ", ", dep_fname) as dependent, 
  concat(substring(dep_ssn,1,3), '-', substring(dep_ssn,4,2), '-', substring(dep_ssn,6,4)) as dep_ssn,
  concat(dep_street, ", ", dep_state, " ", substring(dep_zip,1,5), '-', substring(dep_zip,6,4)) as address,
  concat('(', substring(dep_phone,1,3), ')',substring(dep_phone,4,3), '-', substring(dep_phone,7,4)) as phone_num
FROM employee
  NATURAL LEFT OUTER JOIN dependent
ORDER BY emp_lname;

-- 7) Create "after insert on employee" trigger that automatically creates an audit record in the emp_hist table
drop trigger if exists trg_employee_after_insert;
delimiter //
create trigger trg_employee_after_insert
  AFTER INSERT on employee
  FOR EACH ROW
  BEGIN
  INSERT into emp_hist
  (emp_id, eht_date, eht_type, eht_job_id, eht_emp_salary, eht_usr_changed, eht_reason, eht_notes)
  values (NEW.emp_id, now(), 'i', NEW.job_id, NEW.emp_salary, user(), "new employee", NEW.emp_notes);
  END //
delimiter ;
# LIS3781 - Advanced Database Management

## Benjamin Landerman

### Assignment 1 Requirements:

*Five parts:*

1. Distributed Version Control with Git and Bitbucket
2. AMPPS installation
3. Questions
4. ERD & SQL Code
5. Bitbucket repo links:
    - a) this assignment and
    - b) the completed tutorial (bitbucketstationlocations)

#### A1 Database Business Rules:

The human resource (HR) department of the ACME company wants to contract a database modeler/designer to collect the following employee data for tax purposes:job description, length of employment, benefits,number ofdependents and their relationships, DOB of both the employee and any respective dependents. In addition, employees’ histories mustbe tracked. Also, include the following business rules:

- Each employee may have one or more dependents.
- Each employee has only one job.
- Each job can be held by many employees.
- Many employees may receive many benefits.
- Many benefitsmay be selected by many employees(though, while they may not select anybenefits—any dependentsof employees may be on anemployee’s plan)
- Notes:
- Employee/Dependenttables must use suitable attributes (See Assignment Guidelines);

In Addition:

- Employee: SSN, DOB, start/end dates, salary;
- Dependent: same information as their associated employee(though, not start/end dates),date added(as dependent),type of relationship: e.g., father, mother, etc.
- Job: title(e.g., secretary, service tech., manager, cashier, janitor, IT, etc.)
- Benefit: name(e.g., medical, dental, long-term disability, 401k, term life insurance, etc.)
- Plan: type(single, spouse, family), cost, election date(plans must be unique)
- Employee history:jobs, salaries, and benefit changes, as well as whomade the changeand why;
- Zero Filled data: SSN, zip codes (not phone numbers: US area codesnot below 201, NJ);
- *All* tables must include notesattribute.

#### README.md file should include the following items:

- Screenshot of A1 ERD
- Ex1. SQL Solution
- git commands w/ short descriptions

#### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit -  Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git clone - Clone a repository into a new directory

#### Assignment Screenshots:

| *Screenshot of AMPPS running*                 | *Screenshot of A1 ERD*                        |
|:---------------------------------------------:|:---------------------------------------------:|
|![AMPPS Installation Screenshot](img/ampps.png)|![A1 ERD](img/a1_erd.png)                      |

| *Screenshot of A1 Ex1*                        |
|:---------------------------------------------:|
|![A1 Ex1](img/a1_ex1.png)                      |

#### SQL Links:

[A1 MWB](docs/a1_erd.mwb "Link to A1 MWB")

[A1 SQL](docs/a1_sql.sql "Link to A1 SQL")

[A1 SQL Statements](docs/a1_sql_solutions.sql "Link to A1 SQL Statements")

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/bdl16/bitbucketstationlocations/ "Bitbucket Station Locations")